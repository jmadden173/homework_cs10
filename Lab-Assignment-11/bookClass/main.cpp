
//
//  main.cpp
//  FunWithClasses
//
//  Created by John Stuart on 11/1/17.
//  Copyright � 2017 John Stuart. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
using namespace std;

class Book
{
private:
    //Decleration of Variables
    string title;
    string author;
    string isbn10;
    string isbn13;
    int binding;
    string publisher;
    int pageCount;
    string description;
    string publishDate;
    string edition;

public:
    const static int BindingCount = 4;
    enum   Binding {Unknown, Hardback, Paperback, Electronic};
    //const static string BindingStrings[BindingCount] = {"Unkown", "Hardback", "Paperback", "Electronic"};
    const static string BindingStrings[];

    //Book(string title, string author, string isbn10, string isbn13, int binding, string publisher, int pageCount, string description, string publishDate, string edition)
    Book()
    {
        title       = "";
        author      = "";
        isbn10      = "";
        isbn13      = "";
        binding     = Hardback;
        publisher   = "";
        pageCount   = 0;
        description = "";
        publishDate = "";
        edition = "";

    }

    Book(string title, string author, string isbn10, string isbn13, int binding, string publisher, int pageCount, string description, string publishDate, string edition)
    {

    }

    //Accessors
    //-----------------------

    /*

     Function:

     Description:

     Preconditions: N/A

     Postconditons: N/A

     Parameters: N/A

     Calls: N/A

     Return:

    */

    /*

     Function: getTitle

     Description: Returns the title of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the title variable

     Parameters: N/A

     Calls: N/A

     Return: Title variable from inside the class

    */
    string getTitle(){
    return title;}

    /*

     Function: getAuthor

     Description: Returns the author of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the author variable

     Parameters: N/A

     Calls: N/A

     Return: Author variable from inside the class

    */
    string getAuthor(){
    return author;}

    /*

     Function: getISBN

     Description: Returns the ISBN of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the isbn10 variable

     Parameters: N/A

     Calls: N/A

     Return: ISBN variable from inside the class

    */
    string getISBN(){
    return isbn10;}

    /*

     Function: getISBN13

     Description: Returns the ISBN13 of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: returns the isbn13 variable

     Parameters: N/A

     Calls: N/A

     Return: ISBN13 variable from inside the class

    */
    string getISBN13(){
    return isbn13;}

    /*

     Function: getBinding

     Description: Returns the binding of the book that was inputed into the class, or Hardback for default constructor.

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the binding variable

     Parameters: N/A

     Calls: N/A

     Return: Binding variable from inside the class

    */
    int getBinding(){
    return binding;}

    /*

     Function: getPublisher

     Description: Returns the publisher of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the publisher variable 

     Parameters: N/A

     Calls: N/A

     Return: Publisher variable from inside the class

    */
    string getPublisher(){
    return publisher;}

    /*

     Function: getPageCount

     Description: Returns the page count of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the pageCount variable

     Parameters: N/A

     Calls: N/A

     Return: getPageCount variable from inside the class

    */
    int getPageCount(){
    return pageCount;}

    /*

     Function: getDescription

     Description: Returns the description of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the description variable

     Parameters: N/A

     Calls: N/A

     Return: Description variable from inside the class

    */
    string getDescription(){
    return description;}

    /*

     Function: getPublishDate

     Description: Returns the publish date of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the publishDate variable

     Parameters: N/A

     Calls: N/A

     Return: Publish Date variable from inside the class

    */
    string getPublishDate(){
    return publishDate;}

    /*

     Function: getEdition

     Description: Returns the edition of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Returns the edition variable

     Parameters: N/A

     Calls: N/A

     Return: Edition variable from inside the class

    */
    string getEdition(){
    return edition;}

    /*

     Function: printDetails

     Description: Prints out all of the variables within the book class

     Preconditions: The class of the object has been created

     Postconditons: Output all vairables that are defined in the Book class

     Parameters: N/A

     Calls: getTitle, getAuthor, getISBN, getISBN13, getPublisher, getPageCount, getPublishDate, getEdition, getDescription

     Return: N/A

    */
    void printDetails(){
        cout << "----------BOOK----------\n" ;
        cout << "Title:        " << getTitle() << endl;
        cout << "Author:       " << getAuthor() << endl;
        cout << "ISBN:         " << getISBN() << endl;
        cout << "ISBN13:       " << getISBN13() << endl;
        cout << "Binding:      " << BindingStrings[getBinding()] << endl;
        cout << "Publisher:    " << getPublisher() << endl;
        cout << "Page Count:   " << getPageCount() << endl;
        cout << "Publish Date: " << getPublishDate() << endl;
        cout << "Edition:      " << getEdition() << endl;
        cout << "Description:  " << getDescription() << endl;
        cout << "\n";
    }


    //Mutators
    //--------------------------------------------------------------
    /*

     Function: setTitle

     Description: Sets the title of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return: N/A

    */
    void setTitle(string input){
    Book::title = input;}

    /*

     Function: setAuthor

     Description: Sets the author of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return: N/A

    */
    void setAuthor(string input){
    Book::author = input;}

	/*

     Function: setISBN

     Description:  Sets the isbn10 of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setISBN(string input){
    Book::isbn10 = input;}

	/*

     Function: setISBN13

     Description: Sets the isbn13 of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setISBN13(string input){
    Book::isbn13 = input;}

	/*

     Function: setBinding

     Description: Sets the binding of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setBinding(Binding input){
    Book::binding = input;}

	/*

     Function: setPublisher

     Description: Sets the publisher of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setPublisher(string input){
    Book::publisher = input;}

	/*

     Function: setPageCount

     Description: Sets the pageCount of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setPageCount(int input){
    Book::pageCount = input;}

	/*

     Function: setDescription

     Description: Sets the description of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setDescription(string input){
    Book::description = input;}

	/*

     Function: setPublishDate

     Description: Sets the publishDate of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setPublishDate(string input){
    Book::publishDate;}

	/*

     Function: setEdition

     Description: Sets the edition of the book that was inputed into the class, or none for default constructor

     Preconditions: Needs to have variable properly defined

     Postconditons: Variable is set

     Parameters: string input - the input from the user in which to set the variable in the class to

     Calls: N/A

     Return:

    */
    void setEdition(string input){
    Book::edition;}

};

//const string Book::BindingStrings[Book::BindingCount] = {"Unkown", "Hardback", "Paperback", "Electronic"};

int main(int argc, const char * argv[]) {
    // client code goes here
    string inputString;

    // read a file of books
    // for each book read create a book
    // enter the information
    // print the book data individually
    // print using details



    // create a book
    Book myBook;
    // The book has no title
    cout << myBook.getTitle() << endl;

    // set the title and print it out
    myBook.setTitle("Starting out with C++");
    cout << myBook.getTitle() << endl;

	//Sets variables within the class
    myBook.setTitle("title");
    myBook.setAuthor("author");
    myBook.setISBN("isbn10");
    myBook.setISBN13("isbn13");
    myBook.setBinding(Book::Hardback);
    myBook.setPublisher("publisher");
    myBook.setPageCount(99);
    myBook.setDescription("description");
    myBook.setEdition("2nd");

	//Output all of the variables within the class
    cout << myBook.getTitle() << endl;
    cout << myBook.getAuthor() << endl;
    cout << myBook.getISBN() << endl;
    cout << myBook.getISBN13() << endl;
    cout << myBook.getBinding() << endl;
    cout << myBook.getPublisher() << endl;
    cout << myBook.getPageCount() << endl;
    cout << myBook.getEdition() << endl;

	//Create a new book class with these variables then print details
    Book book2("title", "author", "isbn", "isbn13", Book::Paperback, "publisher", 100, "description", "publishDate", "1st");
    book2.printDetails();


    fstream inputFile;
    // open the file for reading
    inputFile.open("C:/Users/User/Desktop/books.txt");
    if (inputFile)  // if the file opened without error
    {
        while(true)
        {
            // Read title
            if (getline(inputFile, inputString))
                myBook.setTitle(inputString);
            else
                break;  // when we run out of books stop reading them
            // Read author
            if (getline(inputFile, inputString))
                myBook.setAuthor(inputString);

            // Read ISBN
            if (getline(inputFile, inputString))
                myBook.setISBN(inputString);

            // Read ISBN13
            if (getline(inputFile, inputString))
                myBook.setISBN13(inputString);

            // Read Binding
            if (getline(inputFile, inputString))
                for (int s = 0; s < Book::BindingCount; s++)
                {
                    // compare strings, if equal set to enum value
                    if (Book::BindingStrings[s].compare(inputString) == 0)
                        myBook.setBinding(static_cast<Book::Binding>(s));
                }

            // Read Publisher
            if (getline(inputFile, inputString))
                myBook.setPublisher(inputString);

            // Read PageCount
            if (getline(inputFile, inputString))
                //myBook.setPageCount(atoi(inputString));

            // Read Description
            if (getline(inputFile, inputString))
                myBook.setDescription(inputString);

            // Read Publish Date
            if (getline(inputFile, inputString))
                myBook.setPublishDate(inputString);

            // Read Edition
            if (getline(inputFile, inputString))
                myBook.setEdition(inputString);

            cout << "----------BOOK----------\n" ;
            cout << "Title:        " << myBook.getTitle() << endl;
            cout << "Author:       " << myBook.getAuthor() << endl;
            cout << "ISBN:         " << myBook.getISBN() << endl;
            cout << "ISBN13:       " << myBook.getISBN13() << endl;
            cout << "Binding:      " << Book::BindingStrings[myBook.getBinding()] << endl;
            cout << "Publisher:    " << myBook.getPublisher() << endl;
            cout << "Page Count:   " << myBook.getPageCount() << endl;
            cout << "Publish Date: " << myBook.getPublishDate() << endl;
            cout << "Edition:      " << myBook.getEdition() << endl;
            cout << "Description:  " << myBook.getDescription() << endl;
            cout << "\n" ;
        } // end while
        inputFile.close();
    }
    else
    {
        cout << "Could not find the book.txt file" << endl;
    }
    return 0;
}

