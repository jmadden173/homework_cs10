/*

 ScoreCards.cpp

 This file and the companion ScoreCards.h have various functions to return card states
 including:
    Win or Lose
    Type of Win such as Royal Flush or Two Pair.
    Print out the Cards
 It is intended to be used to score poker hands for labs for Santa Rosa Junior Colleges CS10 Course Labs

 Created by John Stuart on 10/5/17.
 Copyright © 2017 John Stuart. All rights reserved.

 */

#include <iostream>
#include <fstream>
#include <cmath>
#include "ScoreCards.h"
#include <stdio.h>
#include <time.h>
#include <cstdlib>

using namespace std;

/*
 Function:      isFlush

 Description:   Determine if 5 suits comprise a flush

 Preconditions:  The values are assumed to be valid suit variables

 Postconditons:  Return value is returned

 Parameters:    int suit1  The suit of the first card
 int suit2  ...
 int suit3  ...
 int suit4  ...
 int suit5  The suit of the fifth card

 Return:        boolean     True  if all suits match
 False if all suits are not matching
 */
bool isFlush(int suit1, int suit2, int suit3, int suit4, int suit5)
{
    // Look for a flush, all suites must be equal so just return if that is true or false
    return (suit1 == suit2 && suit2 == suit3 && suit3 == suit4 && suit4 == suit5);
}

/*
 Function:      isStraight

 Description:   Determine if the ranks of 5 cards form a straight

 Preconditions:     The ranks are assumed to be valid card variables
 The ranks have already been checked to ensure pair is false

 Postconditons:  Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True  if it is a straight (each card is an adjacent rank to the other)
 False if it is not a straight
 */
bool isStraight(int card1, int card2, int card3, int card4, int card5)
{
    bool straight= false;
    // We know there is no pair, so we can assume no duplicates
    // if one card is an ace, repeat with 14

    // thinking if any card is more than 1 away from any other not a straight


    straight =    !((abs(card1 - card2) > 4)   // 12
                    ||  (abs(card1 - card3) > 4)   // 13
                    ||  (abs(card1 - card4) > 4)   // 14
                    ||  (abs(card1 - card5) > 4)   // 15
                    ||  (abs(card2 - card3) > 4)   // 23
                    ||  (abs(card2 - card4) > 4)   // 24
                    ||  (abs(card2 - card5) > 4)   // 25
                    ||  (abs(card3 - card4) > 4)   // 34
                    ||  (abs(card3 - card5) > 4)   // 35
                    ||  (abs(card4 - card5) > 4)); // 45

    if (!straight)
    {
        // If any of the card is an Ace try it high (14)
        if (card1 == 1)
            card1 = 14;
        if (card2 == 1)
            card2 = 14;
        if (card3 == 1)
            card3 = 14;
        if (card4 == 1)
            card4 = 14;
        if (card5 == 1)
            card5 = 14;

        straight =       !((abs(card1 - card2) > 4)   // 12
                           ||  (abs(card1 - card3) > 4)   // 13
                           ||  (abs(card1 - card4) > 4)   // 14
                           ||  (abs(card1 - card5) > 4)   // 15
                           ||  (abs(card2 - card3) > 4)   // 23
                           ||  (abs(card2 - card4) > 4)   // 24
                           ||  (abs(card2 - card5) > 4)   // 25
                           ||  (abs(card3 - card4) > 4)   // 34
                           ||  (abs(card3 - card5) > 4)   // 35
                           ||  (abs(card4 - card5) > 4)); // 45

    }
    return straight;
}


/*
 Function:      isRoyal

 Description:   Determine if the ranks of 5 cards form a Royal flush

 Preconditions:  The values are assumed to be valid suit variables\
 The cards represent a Straight

 Postconditons:  Return value is returned

 Parameters:    int suit1  The suit of the first card
 int suit2  ...
 int suit3  ...
 int suit4  ...
 int suit5  The suit of the fifth card

 Return:        boolean     True  if it is a Royal Straight
 False if it is not
 */
bool isRoyal(int card1, int card2, int card3, int card4, int card5)
{
    //if both an ace and a king are present royal
    return  (
             ((card1 == 1)
              ||(card2 == 1)
              ||(card3 == 1)
              ||(card4 == 1)
              ||(card5 == 1))
             &&
             ((card1 == 13)
              ||(card2 == 13)
              ||(card3 == 13)
              ||(card4 == 13)
              ||(card5 == 13)));

}


/*
 Function:      isFourOfAKind

 Description:   Determine if the ranks of 5 cards Contain 4 of the same rank

 Preconditions:  The ranks are assumed to be valid card variables

 Postconditons:  Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True  if there are 4 matching ranks
 False if there are not 4 matching ranks
 */
bool isFourOfAKind(int card1, int card2, int card3, int card4, int card5)
{
    // There are 5 combinations as shown, if any is true return true
    return ((card1 == card2 && card2 == card3 && card3 == card4) // 1 2 3 4
            || (card1 == card2 && card2 == card3 && card3 == card5)   // 1 2 3 5
            || (card1 == card2 && card2 == card4 && card4 == card5)   // 1 2 4 5
            || (card1 == card3 && card3 == card4 && card4 == card5)   // 1 3 4 5
            || (card2 == card3 && card3 == card4 && card4 == card5)); // 2 3 4 5
}


/*
 Function:      isThreeOfAKind

 Description:   Determine if the ranks of 5 cards Contain 3 of the same rank

 Preconditions: The ranks are assumed to be valid card variables
 The ranks have already been checked to ensure 4 of a kind is false
 Postconditons: Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True  if there are 3 matching ranks
 False if there are not 3 matching ranks
 */
bool isThreeOfAKind(int card1, int card2, int card3, int card4, int card5)
{
    // There are 10 combinations if any are true return true
    return ((card1 == card2 && card2 == card3)   // 123
            ||  (card1 == card2 && card2 == card4)   // 124
            ||  (card1 == card2 && card2 == card5)   // 125
            ||  (card1 == card3 && card3 == card4)   // 134
            ||  (card1 == card3 && card3 == card5)   // 135
            ||  (card1 == card4 && card4 == card5)   // 145
            ||  (card2 == card3 && card3 == card4)   // 234
            ||  (card2 == card3 && card3 == card5)   // 235
            ||  (card2 == card4 && card4 == card5)   // 245
            ||  (card3 == card4 && card4 == card5)); // 345

}

/*
 Function:      isFullHouse

Description:   Determine if the ranks of 5 cards Contain 3 of the same rank and the remaining 2 are of the same rank

 Preconditions: The ranks are assumed to be valid card variables
 The ranks have already been checked to ensure 4 of a kind is false
 The ranks have already been checked to ensure 3 of a kind is true

 Postconditons:  Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True if there are 3 matching ranks and 2 distinct ranks
 False there are not 3 matching ranks and 2 distinct ranks
 */

bool isFullHouse(int card1, int card2, int card3, int card4, int card5)
{
    // Precondition:  three of a kind must already be true
    // if there are three distinct cards then it is not a full house
    // There are 10 combinations if any are true return False

    return  !(   (card1 != card2 && card2 != card3 && card1 != card3)   // 123
              ||  (card1 != card2 && card2 != card4 && card1 != card4)   // 124
              ||  (card1 != card2 && card2 != card5 && card1 != card5)   // 125
              ||  (card1 != card3 && card3 != card4 && card1 != card4)   // 134
              ||  (card1 != card3 && card3 != card5 && card1 != card5)   // 135
              ||  (card1 != card4 && card4 != card5 && card1 != card5)   // 145
              ||  (card2 != card3 && card3 != card4 && card2 != card4)   // 234
              ||  (card2 != card3 && card3 != card5 && card2 != card5)   // 235
              ||  (card2 != card4 && card4 != card5 && card2 != card5)   // 245
              ||  (card3 != card4 && card4 != card5 && card3 != card5)); // 345
}



/*
 Function:      isTwoPair

 Description:   Determine if the ranks of 5 cards Contain 2 sets of 2 cards of the same rank

 Preconditions: The ranks are assumed to be valid card variables
 The ranks have already been checked to ensure 3 of a kind is false

 Postconditons: Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True  if there are 2 sets of matching ranks
 False if there are not 2 sets of matching ranks
 */
bool isTwoPair(int card1, int card2, int card3, int card4, int card5)
{
    //two of a kind can be one of 22 combinations, but half are duplicate
    // There are 11 combinations if any are true return true
    return     ((card1 == card2 && card3 == card4)   // 12 34
            ||  (card1 == card2 && card3 == card5)   // 12 35
            ||  (card1 == card2 && card4 == card5)   // 12 45
            ||  (card1 == card3 && card2 == card4)   // 13 24
            ||  (card1 == card3 && card2 == card5)   // 13 25
            ||  (card1 == card3 && card4 == card5)   // 13 45
            ||  (card1 == card4 && card2 == card3)   // 14 23
            ||  (card1 == card4 && card2 == card5)   // 14 25
            ||  (card1 == card5 && card2 == card3)   // 15 23
            ||  (card1 == card5 && card2 == card4)   // 15 24
            ||  (card1 == card5 && card3 == card4)); // 15 34

}



/*
 Function:      isPair

 Description:   Determine if the ranks of 5 cards Contain 2 cards of the same rank

 Preconditions: The ranks are assumed to be valid card variables
 The ranks have already been checked to ensure 3 of a kind is false
 The ranks have already been checked to ensure 2 pair is false

 Postconditons: Return value is returned

 Parameters:    int card1  The rank of the first card
 int card2  ...
 int card3  ...
 int card4  ...
 int card5  The rank of the fifth card

 Return:        boolean     True  if there are 2  matching ranks
 False if there are not 2  matching ranks
 */
bool isPair(int card1, int card2, int card3, int card4, int card5)
{
    // There are 10 combinations if any are true return true
    return         ((card1 == card2)   // 12
                ||  (card1 == card3)   // 13
                ||  (card1 == card4)   // 14
                ||  (card1 == card5)   // 15
                ||  (card2 == card3)   // 23
                ||  (card2 == card4)   // 24
                ||  (card2 == card5)   // 25
                ||  (card3 == card4)   // 34
                ||  (card3 == card5)   // 35
                ||  (card4 == card5)); // 45

}


/*
 Function:      rankHand

 Description:   Determine if the set of 5 cards comprises a poker win, or loss

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards
                The enumerated type Wins must be present in the file

 Postconditons: The highest poker hand win value is returned

 Parameters:    int card1  The rank of the first card
                int suit1  The suit of the first card
                int card2  ...
                int suit2  ...
                int card3  ...
                int suit3  ...
                int card4  ...
                int suit4  ...
                int card5  The rank of the fifth card
                int suit5  The suit of the fifth card

 Return:        Wins  The type of win that has occured based on the Enumerated type Wins
 */
Wins rankHand(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5)
{
    bool flush = false;
    bool straight = false;
    bool royal = false;
    bool four = false;
    bool full = false;
    bool three = false;
    bool pair = false;
    bool twoPair = false;

    // Suit Related Wins
    // first look for a flush
    flush = isFlush(suit1, suit2, suit3, suit4, suit5);

    //Value Related Wins
    // is Four of a kind
    four = isFourOfAKind(card1, card2, card3, card4, card5);

    // is Three of a kind
    if (!four) // 4 beats 3
        three = isThreeOfAKind(card1, card2, card3, card4, card5);

    // Full House
    if (three) // requires 3 of a kind automatically leaves out 4 of a kind
        full = isFullHouse(card1, card2, card3, card4, card5);

    // is 2 pair
    if (!three) // 3 beats 2 pair
        twoPair = isTwoPair(card1, card2, card3, card4, card5);

    // is pair
    if (!three && !twoPair) // 3 beats 2 pair beats a pair
        pair = isPair(card1, card2, card3, card4, card5);

    // is pair
    if (!four && !three && !twoPair && !pair) // 3 beats 2 pair beats a pair
        straight = isStraight(card1, card2, card3, card4, card5);

    if (straight)
        royal = isRoyal(card1, card2, card3, card4, card5);


    // Display ranks
    // RoyalFlush, StraightFlush, FourOfAKind, FullHouse, Flush, Straight, ThreeOfAkind, TwoPair, Pair
    if (four)
        return FourOfAKind;

    if (full)
    {
        return FullHouse;
    }
    else if (three) // not both
    {
        return ThreeOfAkind;
    }

    // Royal Flush
    if (flush && royal)
        return RoyalFlush;

    // Straight Flush
    if (flush && straight && !royal)
        return StraightFlush;


    // Flush
    if (flush && !royal && !straight)
        return Flush;

    // Straight
    if (straight && !flush)
        return Straight;

    // Two Pair
    if (twoPair)
    {
        return TwoPair;
    }

    // One Pair
    if (pair)
    {
        return Pair;
    }

    return NoWinner;

}

/*
 Function:      isHandUnique

 Description:   Verify that a hand is unique

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The ASCII art of this deck is printed to stdout
                The highest rank of the card deck is printed out to stdout

 Parameters:    int card1  The rank of the first card
                int suit1  The suit of the first card
                int card2  ...
                int suit2  ...
                int card3  ...
                int suit3  ...
                int card4  ...
                int suit4  ...
                int card5  The rank of the fifth card
                int suit5  The suit of the fifth card

 Return:        True if the hand is unique
                False if the hand has a duplicate card
 */
bool isHandUnique(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5)
{
             return            !((card1 == card2 && suit1 == suit2)   // 12
                             ||  (card1 == card3 && suit1 == suit3)   // 13
                             ||  (card1 == card4 && suit1 == suit4)   // 14
                             ||  (card1 == card5 && suit1 == suit5)   // 15
                             ||  (card2 == card3 && suit2 == suit3)   // 23
                             ||  (card2 == card4 && suit2 == suit4)   // 24
                             ||  (card2 == card5 && suit2 == suit5)   // 25
                             ||  (card3 == card4 && suit3 == suit4)   // 34
                             ||  (card3 == card5 && suit3 == suit5)   // 35
                             ||  (card4 == card5 && suit4 == suit5)); // 45
}


/*
 Function:      drawHand

 Description:   draws the hand to stdout

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The card deck is printed out to stdout in ANSI art form

 Parameters:    int card1  The rank of the first card
                int suit1  The suit of the first card
                int card2  ...
                int suit2  ...
                int card3  ...
                int suit3  ...
                int card4  ...
                int suit4  ...
                int card5  The rank of the fifth card
                int suit5  The suit of the fifth card

 Return:        Void
*/
void drawHand(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5)
{
    int numCards = 5;

    drawHeader(numCards);

    drawValue(card1, suit1);
    drawValue(card2, suit2);
    drawValue(card3, suit3);
    drawValue(card4, suit4);
    drawValue(card5, suit5);
    cout << endl;
    drawMiddle(numCards);
    drawMiddle(numCards);
    drawMiddle(numCards);
    drawMiddle(numCards);

    drawValue(card1, suit1, false);
    drawValue(card2, suit2, false);
    drawValue(card3, suit3, false);
    drawValue(card4, suit4, false);
    drawValue(card5, suit5, false);

    cout << endl;
    drawFooter(numCards);

}


/*
 Function:      drawHeader

 Description:   draws the decks header  to stdout

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The card header is printed out to stdout in ANSI art form

 Parameters:    int numCards the number of Cards in the hand

 Return:        Void
*/
void drawHeader(int numCards)
{
    for (int t = 0; t < numCards; t++)
        cout << "/----------\\ ";
    cout << endl;
}

/*
 Function:      drawFooter

 Description:   draws the decks footer  to stdout

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The card footer is printed out to stdout in ANSI art form

 Parameters:    int numCards the number of Cards in the hand

 Return:        Void
 */
void drawFooter(int numCards)
{
    for (int t = 0; t < numCards; t++)
        cout << "\\----------/ ";
    cout << endl;
}

/*
 Function:      drawMiddle

 Description:   draws the decks center  to stdout

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The card center is printed out to stdout in ANSI art form

 Parameters:    int numCards the number of Cards in the hand

 Return:        Void
 */
void drawMiddle(int numCards)
{
    for (int t = 0; t < numCards; t++)
        cout << "|          | ";
    cout << endl;
}

/*
 Function:      drawValue

 Description:   draws the decks rank and suit to stdout

 Preconditions: The ranks and suits are assumed to be valid card variables of 5 distinct cards

 Postconditons: The card rank an suit is printed out to stdout in ANSI art form

 Parameters:    int card the rank to draw
                int suit the suit to draw
                bool left is this left justified or right justified, true is left justified

 Return:        Void
 */
void drawValue(int card, int suit, bool left /*=true*/)

{
    // output left border
    if (left)
        cout << "| ";
    else
        cout << "|     ";
    // output the rank
    switch (card)
    {
        case Ace:
            cout << " A";
            break;

        case Deuce:
            cout << " 2";
            break;

        case Three:
            cout << " 3";
            break;

        case Four:
            cout << " 4";
            break;

        case Five:
            cout << " 5";
            break;

        case Six:
            cout << " 6";
            break;

        case Seven:
            cout << " 7";
            break;

        case Eight:
            cout << " 8";
            break;

        case Nine:
            cout << " 9";
            break;

        case Ten:
            cout << "10";
            break;

        case Jack:
            cout << " J";
            break;

        case Queen:
            cout << " Q";
            break;

        case King:
            cout << " K";
            break;

        default:
            cout << endl << "Something went terribly wrong!" << endl;
    }

    // output the suit
    switch (suit)
    {
        case Clubs:
            cout << "C";
            break;

        case Spades:
            cout << "S";
            break;

        case Diamonds:
            cout << "D";
            break;

        case Hearts:
            cout << "H";
            break;

        default:
            cout << endl << "Something went terribly wrong!" << endl;
    }

    // output the right border
    if (left)
        cout <<"      | ";
    else
        cout <<"  | ";
}

/* Lab assignment 7 is to complete the below function, and to document it according to the function
 documentation used with the above functions.
 Part of this has been done for you.
 */



/*
 Function:	printRank

 Description:	prints the rank of the card set

 Preconditions: needs to be called along with the "ScoreCards.h" to be included

 Postconditons: outpued to console what rank the cards are

 Parameters: int card1-card10 in order = the hand of cards that the player has right now

 Calls:  drawHand
         rankHand

 Return:        Void
 */
void printRank(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5)
{
	//Variables
	//Define class win
    Wins win;
    drawHand(card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5);
	//Switch statement depending on the the parameters passed to the function
    switch(rankHand(card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5))
    {
    case 0:
        cout << "Royal Flush" << endl;
        break;
    case 1:
        cout << "Straight Flush" << endl;
        break;
    case 2:
        cout << "Four of a Kind" << endl;
        break;
    case 3:
        cout << "Full House" << endl;
        break;
    case 4:
        cout << "Flush" << endl;
        break;
    case 5:
        cout << "Straight" << endl;
        break;
    case 6:
        cout << "Three of a kind" << endl;
        break;
    case 7:
        cout << "Two Pair" << endl;
        break;
    case 8:
        cout << "Pair" << endl;
        break;
    case 9:
        cout << "No winner :(" << endl;
        break;
    }
    cout << endl;
    }
