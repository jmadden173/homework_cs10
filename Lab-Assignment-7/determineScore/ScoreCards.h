/*

 ScoreCards.h

 This file and the companion ScoreCards.cpp have various functions to return card states
 including:
    Win or Lose
    Type of Win such as Royal Flush or Two Pair.
    Print out the Cards
 It is intended to be used to score poker hands for labs for Santa Rosa Junior Colleges CS10 Course Labs
 
 Created by John Stuart on 10/5/17.
 Copyright © 2017 John Stuart. All rights reserved.

 */


#ifndef ScoreCards_h
#define ScoreCards_h

// Enumeration for Suits and Cards and Wins
enum Suits {Clubs=1, Spades, Diamonds, Hearts};
enum Cards {Ace = 1, Deuce, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};
enum Wins  {RoyalFlush, StraightFlush, FourOfAKind, FullHouse, Flush, Straight, ThreeOfAkind, TwoPair, Pair, NoWinner};

// functions to determine if there is a winning hand
bool isFlush(int suit1, int suit2, int suit3, int suit4, int suit5);
bool isStraight(int card1, int card2, int card3, int card4, int card5);
bool isRoyal(int card1, int card2, int card3, int card4, int card5);
bool isFourOfAKind(int card1, int card2, int card3, int card4, int card5);
bool isThreeOfAKind(int card1, int card2, int card3, int card4, int card5);
bool isFullHouse(int card1, int card2, int card3, int card4, int card5);
bool isTwoPair(int card1, int card2, int card3, int card4, int card5);
bool isPair(int card1, int card2, int card3, int card4, int card5);

// Rank the hand
Wins rankHand(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5);

// Is the hand unique (no two cards of the same suit and rank
bool isHandUnique(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5);

// Functions to print out the deck
void printRank(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5);
void drawHand(int card1, int suit1, int card2, int suit2, int card3, int suit3, int card4, int suit4, int card5, int suit5);
void drawHeader(int numCards);
void drawFooter(int numCards);
void drawMiddle(int numCards);
void drawValue(int card, int suit, bool left=true);



#endif /* ScoreCards_h */
