#include <iostream>
#include <fstream>
#include <cmath>
#include "ScoreCards.h"
#include <time.h>
#include <cstdlib>

using namespace std;

void quit(string msg)
{
    cout << msg << endl;
    exit (1);
}



int main()
{
    //Suits suitin;
    //Cards cardin;

    int suit1 = 0, suit2 = 0, suit3 = 0, suit4 = 0, suit5 = 0;
    int card1 = 0, card2 = 0, card3 = 0, card4 = 0, card5 = 0;

    suit1=suit2=suit3=suit4=suit5=Hearts;
    card1=card2=card3=card4=card5=Ten;


    printRank(13,4, 11,4, 10,4, 12,4,  1,4); // Royal Flush
    printRank( 1,3, 11,3, 13,3, 12,3,  10,3); // Royal Flush

    printRank( 1,2,  2,2,  3,1,  4,3,  5,3); // Straight

    printRank(1,1, 2,1, 8,1, 4,1, 5,2); // Nothing


    printRank( 1,2,  2,2,  3,2,  4,2,  5,2); // Straight Flush

    printRank( 1,2,  2,2,  3,1,  4,2,  5,3); // Straight

    printRank( 1,1,  1,2,  1,3,  1,4,  5,1); // four of a kind
    printRank( 5,1,  1,2,  1,3,  1,4,  1,1); // four of a kind
    printRank(11,1, 11,2,  5,1, 11,4, 11,3); // four of a kind

    printRank( 6,1,  1,2,   1,3,  1,4,  6,2); // Full House
    printRank( 5,1,  5,2,   1,3,  1,4,  1,1); // Full House
    printRank(11,1, 12,2,  12,1, 11,4, 11,3); // Full House

    printRank( 1,1,  2,1,  8,1,  4,1,  5,1); // Flush

    printRank(  1,1,  2,2,   3,3,   4,4,   5,1); // Straight
    printRank( 10,1, 11,2,  12,3,  13,4,   1,1); // Straight
    printRank(  6,1,  7,2,   8,3,   9,4,  10,1); // Straight

    printRank( 6,1,  1,2,  1,3,  1,4,  5,1); // three of a kind
    printRank( 5,1,  6,2,  1,3,  1,4,  1,1); // three of a kind
    printRank(11,1, 12,2,  5,1, 11,4, 11,3); // three of a kind

    printRank( 5,1,  6,2,  1,3,  1,4,  5,2); // two pair
    printRank(11,1, 12,2,  5,1, 12,4, 11,3); // two pair

    printRank( 5,1,  6,2,  1,3,  1,4,  2,1); // pair
    printRank(11,1, 12,2,  5,1, 11,4, 1,3); // pair



    // Seed the random function
    srand(unsigned(time(0)));
    // Select 5 cards at random


    do
    {
        card1 = rand() % King + 1;
        suit1 = rand() % 4 + 1;

        card2 = rand() % King + 1;
        suit2 = rand() % 4 + 1;


        card3 = rand() % King + 1;
        suit3 = rand() % 4 + 1;


        card4 = rand() % King + 1;
        suit4 = rand() % 4 + 1;


        card5 = rand() % King + 1;
        suit5 = rand() % 4 + 1;
    }
    while (!isHandUnique(card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5));

    printRank(card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5); // random

    return 0;
}
