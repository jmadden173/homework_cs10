
//  main.cpp
//  Array
//
//  Created by John Madden on 10/25/17.
//  Copyright � 2017 John Madden. All rights reserved.
//

#include <iostream>
using namespace std;


int main(int argc, const char * argv[]) {
	//Variables
    int   intArray[10]; // uninitialized array of integers

    int  intArray2[]={1,2,3,4,5,6,7,8,9,0};

    float floatArray[10]; // unitialized array of floats

    char  charArray[10]={'a','b','c','d','e','f','g','h','i','j'};


    // output the intArray with unset variables
    cout << "Uninitialized Integer Array" << endl;
    //print array contents
    for (int i=0; i<10; i++)
    {
        cout << intArray[i] << " ";
    }
    cout << endl << endl;


    // output the initialized intArray2 values
    cout << "Initialized Integer Array" << endl;
    //print array contents
    for (int i=0; i<10; i++)
    {
        cout << intArray2[i] << " ";
    }
    cout << endl << endl;


    // ouptput the initialized character array values
    cout << "Initialized Character Array" << endl;
    //print array contents
    for (int i=0; i<10; i++)
    {
        cout << charArray[i] << " ";
    }
    cout << endl << endl;


    //set intArray contents to the numbers 100 to 109
    cout << "Modified Character Array" << endl;
    //Print the array
    int countNum = 100;
    for (int i=0; i<10; i++)
    {
        intArray[i] = countNum;
        cout << intArray[i] << " ";
        countNum++;
    }
    cout << endl << endl;


    //set floatArray contents to 0.01 to 0.10
    cout << "Modified Float Array" << endl;
    // Print the Array
    float countFloat = .01;
    for (int i=0; i<10; i++)
    {
        floatArray[i] = countFloat;
        cout << floatArray[i] << " ";
        countFloat+=.01;
    }
    cout << endl << endl;

    return 0;
}



