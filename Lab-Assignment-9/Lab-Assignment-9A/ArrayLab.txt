
//  main.cpp
//  Array
//
//  Created by John Stuart on 10/25/17.
//  Copyright © 2017 John Stuart. All rights reserved.
//

#include <iostream>
using namespace std;


int main(int argc, const char * argv[]) {
    int   intArray[10]; // uninitialized array of integers
    
    int  intArray2[]={1,2,3,4,5,6,7,8,9,0};
    
    float floatArray[10]; // unitialized array of floats
    
    char  charArray[10]={'a','b','c','d','e','f','g','h','i','j'};
    
    
    // output the intArray with unset variables
    cout << "Uninitialized Integer Array" << endl;
    //print array contents
    
    
    // output the initialized intArray2 values
    cout << "Initialized Integer Array" << endl;
    //print array contents
    
    
    // ouptput the initialized character array values
    cout << "Initialized Character Array" << endl;
    //print array contents
    
    
    //set intArray contents to the numbers 100 to 109
    //Print the array
    
    
    
    //set floatArray contents to 0.01 to 0.10
    // Print the Array
    
    
    
    
    
    return 0;
}



