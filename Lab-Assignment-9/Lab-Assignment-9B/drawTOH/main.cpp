//
//  main.cpp
//  TOH
//
//  Created by John Stuart on 10/30/17.
//  Copyright � 2017 John Stuart. All rights reserved.
//

#include <iostream>
using namespace std;

const int PEGS = 3;
const int DISKS = 5;



/*

 Function:          printState

 Description:      prints the state of the state array

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  should output a neat diagram of the TOH

 Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               <What does the function return>

 */
void printState(int state[PEGS][DISKS])
{
	//String which states the different layers that can be outputted
    string drawDisks[DISKS+1] = {
        "      |      ",
        "     ===     ",
        "    =====    ",
        "   =======   ",
        "  =========  ",
        " =========== "
    };

	//Two for loops for the 2D array that is being outputting
    for (int i=0; i<DISKS; i++)
    {
        for (int j=0; j<PEGS; j++)
        {
            cout << drawDisks[state[j][i]];
        }
        cout << endl;
    }
	//Outputs the lower part of the TOH
    cout << "------+------------+------------+------\n|_____________________________________|\n";
}
/*

 Function:           initializeDisks

 Description:      Initially sets the state array

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  set the initial gamestate

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               N/A

 */
void initializeDisks(int state[PEGS][DISKS])
{
    // complete the initialization so that the array looks like this:
    /*
     Remember that arrays are 0 based

     P0 P0 P0
     D0  0  0  1
     D1  0  0  2
     D2  0  0  3
     D3  0  0  4
     D4  0  0  5

     */
	 //Two loops to complete the initialization of entire TOH array
     for (int i=0; i<PEGS-1; i++)
     {
         for (int j=0; j<DISKS; j++)
         {
             state[i][j] = 0;
         }
     }
     for (int j=0; j<DISKS; j++)
     {
         state[PEGS-1][j] = j+1;
     }
}


/*

 Function:           makeMove

 Description:      changes the gamestate 

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  has changed gamestate so that it is what the player wanted

 Parameters:       <List of parameters:  type name  and description>

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               Always returns True for now

 */
bool makeMove(int state[PEGS][DISKS])
{
    return false;
}



/*

 Function:           checkForWin

 Description:      check if the gamestate has won

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  checks if the player has won the game

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               Always returns True for now

 */
bool checkForWin(int state[PEGS][DISKS])
{
    return true;
}


int main(int argc, const char * argv[]) {
	//Variables
    int state[PEGS][DISKS];
    bool winState = false;

    cout << "Welcpme tp the Tower of Hanoi" << endl;
    // add more instructions here

    // complete the initializeDisks function
    initializeDisks(state);

    // complete the printState function
    printState(state);



    return 0;
}
