//
//  main.cpp
//  Scope
//
//  Created by John Stuart on 10/25/17.
//  Copyright � 2017 John Stuart. All rights reserved.
//

#include <iostream>
using namespace std;

int integer = 1000;

/* 

 Function:           <Function Name> 

 Description:      <What does the function do?> 

 Preconditions:  <What has to be in place for this function to work properly> 

 Postconditons:  <What has occurred after this function has finished running> 

 Parameters:       <List of parameters:  type name  and description> 

 Return:               <What does the function return> 

 */ 
 
 /* 

 Function:           printGlobal 

 Description:      Prints the global variable integer 

 Preconditions:  Need to call the function and set integer to a function

 Postconditons:  Printed Integer to console

 Parameters:       N/A

 Return:               N/A

 */ 
void printGlobal()
{
    // complete this code
    cout << integer << endl;

}

/* 

 Function:           printLocal

 Description:      Print out the local version of the integer (It should have the value of 100

 Preconditions:  Need to call the function

 Postconditons:  Local integer will be set to a number and printed out that integer

 Parameters:       N/A

 Return:               N/A

 */ 
void printLocal()
{
    // complete this code
    int integer = 100;
    cout << integer << endl;

}

/* 

 Function:           printStatic

 Description:       Print out the local version of the integer
					Take the variable from the printLocal() fucntion above and
					Make the variable static, and add one to it each iteration so that it returns 100, 101, 102 ...

 Preconditions:  	Function has to be called 

 Postconditons:  Printed out numbers 100-104

 Parameters:       N/A

 Return:               N/A

 */ 
void printStatic()
{
    // complete this code
    for(static int integer = 100; 105>integer; integer++)
    {
        cout << integer << endl;
    }
}

/* 

 Function:           printNumber

 Description:      Takes the number passed to the function and print it

 Preconditions:  Function has to be called and be passed an integer

 Postconditons:  Printed out past variable

 Parameters:       int Number = any number that you want to be printed out

 Return:               N/A

 */ 
void printNumber(int number)
{
    cout << number << endl;
}

int main(int argc, const char * argv[]) {

    printGlobal(); // 1000
    printLocal();  // 100

    cout << "--------------------------\n";

    // Make this integer declaration
    // START HERE
    int integer = 10;

    // Print out the block local version of integer (10)
    printNumber(integer);


    //END HERE


    // Print out the global version of integer This should print out the Global version, not the local version
    // if you did the above part correctly. (1000)
    printGlobal();

    cout << "--------------------------\n";


    // Print out the local version of integer (10)
    printNumber(integer);

    // Print out integer here and it should be the local version (10)
    printNumber(integer);


    cout << "--------------------------\n";


    // now we will illustrate the difference between local and static variables

    for (int t=0; t< 5; t++)
    {
        printLocal();
    }
    /*
     <printLocal> integer = 100
     <printLocal> integer = 100
     <printLocal> integer = 100
     <printLocal> integer = 100
     <printLocal> integer = 100
     */


    cout << "--------------------------\n";
    for (int t=0; t< 5; t++)
    {
        printStatic();
    }
    /*
     <printStatic> integer = 100
     <printStatic> integer = 101
     <printStatic> integer = 102
     <printStatic> integer = 103
     <printStatic> integer = 104
    */


    cout << "--------------------------\n";

    return 0;
}





