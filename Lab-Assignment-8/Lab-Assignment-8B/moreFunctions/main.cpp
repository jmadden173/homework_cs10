//
//  main.cpp
//  FunctionsFinale
//
//  Created by John Madden on 10/30/17.
//  Copyright � 2017 John Madden. All rights reserved.
//

#include <iostream>
#include <stdio.h>
using namespace std;


//printNumber

//addNumbers

//printMulti
//printMulti
//printMulti
//printMulti

//passByValue

//passByReference

/* 

 Function:           <Function Name> 

 Description:      <What does the function do?> 

 Preconditions:  <What has to be in place for this function to work properly> 

 Postconditons:  <What has occurred after this function has finished running> 

 Parameters:       <List of parameters:  type name  and description> 

 Return:               <What does the function return> 

 */
 
 /* 

 Function:           passByReference

 Description:      prints out the integer that was passed by reference and adds one to the integer

 Preconditions:  Has to have the integer defined

 Postconditons:  prints out the integer that was passed by reference and adds one to the integer

 Parameters:       int input = input of the function

 Return:               N/A

 */
void passByReference(int &input)
{
    printf("%i\n", input);
    input++;
}

/* 

 Function:          passByValue 

 Description:      Prints an umber

 Preconditions:  input has to be defined

 Postconditons:  printed out the input variable

 Parameters:       int input = input of the function

 Return:               N/A

 */
void passByValue(int input)
{
    printf("%i\n", input);
}

/* 

 Function:           printMulti

 Description:      prints two number

 Preconditions:  has to be passed two values

 Postconditons:  printed out two integers

 Parameters:       int int1 = first input
					int int2 = second input

 Return:               <What does the function return> 

 */
void printMult(int int1, int int2)
{
    printf("Int, Int : %i\n", (int1*int1));
}

/* 

 Function:           printMulti

 Description:      prints two number

 Preconditions:  has to be passed two values

 Postconditons:  printed out two int

 Parameters:       int int1 = first input
					int int2 = second input

 Return:               <What does the function return> 

 */
void printMult(int int1, float float1)
{
    printf("Int, Float : %f\n", (float1*int1));
}

/* 

 Function:           printMulti

 Description:      prints two number

 Preconditions:  has to be passed two values

 Postconditons:  printed out two numbers

 Parameters:       int int1 = first input
					float float = second input

 Return:               <What does the function return> 

 */
void printMult(float float1, int int1)
{
    printf("Float, Int : %f\n", (float1*int1));
}

/* 

 Function:           printMulti

 Description:      prints two number

 Preconditions:  has to be passed two values

 Postconditons:  printed out floats

 Parameters:       float float1 = first input
					float float2 = second input

 Return:               <What does the function return> 

 */
void printMult(float float1, float float2)
{
    printf("Float, Float : %f\n", (float1*float1));
}

/* 

 Function:           addNumber

 Description:      adds numbers

 Preconditions:  has to be passed at least one integer

 Postconditons:  printed out the sum of all numbers passed

 Parameters:        int int1 = first input
					 int int2 = second input
					  int int3 = third input
					   int int4 = fourth input

 Return:               <What does the function return> 

 */
int addNumbers(int num1, int num2=0, int num3=0, int num4=0)
{
    return(num1+num2+num3+num4);
}

/* 

 Function:           printNumber

 Description:      prints a number

 Preconditions:  Be passed an integer

 Postconditons:  integer should have been printed out

 Parameters:       int input = input to be cout'ed
					bool debug = tells where to print out debug

 Return:               <What does the function return> 

 */
void printNumber(int input, bool debug=false)
{
    if(debug==true)
    {
        cout << "debug ";
    }
    cout << input << endl;
}

int main(int argc, const char * argv[]) {
    /*
     write the printNumber function
     Parameters:  integer (Mandatory parameter)
     boolean (optional parameter)

     So that the below functions will produce the following output

     Optional Parameters
     0
     5
     debug 10
     --------------------------------------
     */

    cout << "Optional Parameters" << endl;
    printNumber(0);
    printNumber(5, false);
    printNumber(10, true);
    cout << "\n--------------------------------------\n";

    /*
     write the addNumber function
     Parameters:  integer (Mandatory parameter)
     integer (optional parameter)
     integer (optional parameter)
     integer (optional parameter)

     So that the below functions will produce the following output

     Optional Parameters Again
     1
     3
     6
     10
     --------------------------------------
     */

    cout << "Optional Parameters Again" << endl;
    cout << addNumbers(1) << endl;
    cout << addNumbers(1,2) << endl;
    cout << addNumbers(1,2,3) << endl;
    cout << addNumbers(1,2,3,4) << endl;
    cout << "\n--------------------------------------\n";



    int integer = 8;
    float floating = 1.3;
    /*
     write the printMulti function
     Parameters:
     4 overloaded functions
     int int
     int float
     float int
     float float
     So that the below functions will produce the following output
     Overloading
     Int, Int : 64
     Int, Float : 10.4
     Float, Int : 10.4
     Float, Float : 1.69
     --------------------------------------
     */

    cout << "Overloading" << endl;
    printMult(integer, integer);
    printMult(integer, floating);
    printMult(floating, integer);
    printMult(floating, floating);

    cout << "\n--------------------------------------\n";
    integer = 1;


    /*
     write the passByValue and PassByReference functions
     Parameters:
     integer

     So that the below functions will produce the following output

     Pass by Value
     1
     1
     1
     1
     1

     -------------
     Pass by Reference
     1
     2
     3
     4
     5

     -------------
     Pass by value after passed by reference
     6
     6
     6
     6
     6
     --------------------------------------
     */
    // pass by value
    cout << "Pass by Value" << endl;
    for (int x = 0; x < 5; x++)
    {
        passByValue(integer);
    }
    cout << "\n-------------\n";


    // pass by reference
    cout << "Pass by Reference" << endl;
    for (int x = 0; x < 5; x++)
    {
        passByReference(integer);
    }
    cout << "\n-------------\n";

    // pass by value
    cout << "Pass by value after passed by reference" << endl;
    for (int x = 0; x < 5; x++)
    {
        passByValue(integer);
    }
    cout << "\n--------------------------------------\n";


    /*
     Using the exit function return 12

     this will produce (your IDE may or may not display this to you)
     Program ended with exit code: 12
     */


    return 0;

}
