//
//  main.cpp
//  TOH
//
//  Created by John Stuart on 10/30/17.
//  Copyright � 2017 John Stuart. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
using namespace std;

//Define constant size of the game
const int PEGS = 3;
const int DISKS = 5;

bool checkForWin(int state[PEGS][DISKS]);

/*

 Function:          printState

 Description:      prints the state of the state array

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  should output a neat diagram of the TOH

 Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               <What does the function return>

 */
void printState(int state[PEGS][DISKS])
{
    string drawDisks[DISKS+1] = {
        "      |      ",
        "     ===     ",
        "    =====    ",
        "   =======   ",
        "  =========  ",
        " =========== "
    };

    for (int i=0; i<DISKS; i++)
    {
        for (int j=0; j<PEGS; j++)
        {
            cout << drawDisks[state[j][i]];
        }
        cout << endl;
    }
    cout << "------+------------+------------+------\n|_____________________________________|\n";
}
/*

 Function:           initializeDisks

 Description:      Initially sets the state array

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  set the initial gamestate

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               N/A

 */
void initializeDisks(int state[PEGS][DISKS])
{
    // complete the initialization so that the array looks like this:
    /*
     Remember that arrays are 0 based

     P0 P0 P0
     D0  0  0  1
     D1  0  0  2
     D2  0  0  3
     D3  0  0  4
     D4  0  0  5

     */
     for (int i=0; i<PEGS-1; i++)
     {
         for (int j=0; j<DISKS; j++)
         {
             state[i][j] = 0;
         }
     }
     for (int j=0; j<DISKS; j++)
     {
         state[PEGS-1][j] = j+1;
     }
}


/*

 Function:           makeMove

 Description:      changes the gamestate 

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  has changed gamestate so that it is what the player wanted

 Parameters:       <List of parameters:  type name  and description>

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               Always returns True for now

 */
bool makeMove(int state[PEGS][DISKS])
{
	//Define local variables
    int moveIn;
    int diskIn;
    int moveOut;
    int diskOut;
    bool gameDone = false;
	//While loop to constantly make moves until the winning move is found
    while (gameDone == false)
        {
			//Asks for the input of where to move from and too
            printf("Enter the peg number to move from: ");
            cin >> moveIn;
            moveIn--;
            printf("Enter the peg number to move to: ");
            cin >> moveOut;
            moveOut--;

			//Check to make sure that they are greater than zero
            if ((moveIn < 0) || (moveOut < 0))
            {
                printf("__________________________________________________\n");
                printf("You entered -1 to exit and therefore lost the game");
                exit(0);
            }
			//Check to make sure they are not equal
            else if (moveIn == moveOut)
            {
                printf("Your move is not valid\n");
            }
			//If it it a valid move continue the move
            else
            {
                for (int i=0; i<DISKS; i++)
                {
                    if (state[moveIn][i]!=0)
                    {
                        diskIn = i;
                        break;
                    }
                }

                for (int i=0; i<DISKS; i++)
                {
                    if (state[moveOut][i]!=0)
                    {
                        diskOut = i;
                        break;
                    }
                    else
                    {
                        diskOut = 4;
                    }
                }

                if (state[moveOut][diskOut]!=0)
                {
                    if ((state[moveIn][diskIn] - state[moveOut][diskOut]) > 0)
                    {
                        printf("Your move is not valid\n");
                    }
                    else if ((state[moveIn][diskIn] - state[moveOut][diskOut]) < 0)
                    {
                        state[moveOut][diskOut-1] = state[moveIn][diskIn];
                        state[moveIn][diskIn] = 0;

                    }



                }
                else if (state[moveOut][diskOut]==0)
                {
                    state[moveOut][diskOut] = state[moveIn][diskIn];
                    state[moveIn][diskIn] = 0;
                }

                printState(state);

                gameDone = checkForWin(state);

            }

        }


    return false;
}


/*

 Function:           checkForWin

 Description:      check if the gamestate has won

 Preconditions:  array has to be PROPERLY DEFINED

 Postconditons:  checks if the player has won the game

Parameters:       int state[PEG][DISK] = an 2d array defined by constants which contains game state

 Return:               Returns is true for false depending on the array

 */
bool checkForWin(int state[PEGS][DISKS])
{
    if (state[0][0] == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}


int main(int argc, const char * argv[]) {
	//Defind local variables
    int state[PEGS][DISKS];
    bool winState = false;

	//Print out the instructions of the TOH game
    cout << "Welcpme tp the Tower of Hanoi" << endl;
	cout << "1. Move only one disk at a time." << endl;
	cout << "2. A larger disk may not be placed ontop of a smaller disk." << endl;
	cout << "3. All disks, except the one being moved, must be on a peg." << endl;

    // complete the initializeDisks function
    initializeDisks(state);

    // complete the printState function
    printState(state);

    makeMove(state);



    return 0;
}
